(require "asdf")

(pushnew :md5-testing *features*)

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "md5"))

;; Run the tests mentioned in .travis.yml
(unless (and (md5::test-rfc1321)
             (md5::test-other))
  (uiop:quit 1))
